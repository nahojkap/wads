package com.worthlessapps.wads;

class BlockConnection {
    public final Block connectedBlock;
    public final BlockDirection blockDirection;

    public BlockConnection(BlockDirection blockDirection, Block connectedBlock) {

        this.blockDirection = blockDirection;
        this.connectedBlock = connectedBlock;
    }
}
