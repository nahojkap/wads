package com.worthlessapps.wads;


import java.util.ArrayList;
import java.util.List;

public class BlockManager
{
    BlockRegion[][] blockRegions;

    int numPixelsPerRegion = 100;

    int width;
    int height;

    public BlockManager(int width, int height, long visibilityDelayMs) {
        this.width = width;
        this.height = height;
        this.visibilityDelayMs = visibilityDelayMs;

        int numRegionsForX = (this.width / this.numPixelsPerRegion) + (this.width % this.numPixelsPerRegion == 0 ? 0 : 1);
        int numRegionsForY = (this.height / this.numPixelsPerRegion) + (this.height % this.numPixelsPerRegion == 0 ? 0 : 1);
        this.blockRegions = new BlockRegion[numRegionsForX][numRegionsForY];
    }

    Block root;
    Block lastVisibleBlock;
    long nextBlockVisibleAt;

    long visibilityDelayMs = 500;

    boolean active = true;

    List<BlockRegion> getBlockRegions(Block block) {

        final List<BlockRegion> matchingBlockRegions = new ArrayList<>();

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; y++)
            {

            }

        }


    }
    void draw() {

        if (active) {
            if (nextBlockVisibleAt < System.currentTimeMillis()) {
                if (lastVisibleBlock.next() != null) {
                    lastVisibleBlock.visible = true;
                    lastVisibleBlock = lastVisibleBlock.next();
                    nextBlockVisibleAt = System.currentTimeMillis() + visibilityDelayMs;
                }
            }
        }

        Block block = this.root;
        while (block.next() != null)
        {
            block.draw();
            block = block.next();
        }
    }
}
