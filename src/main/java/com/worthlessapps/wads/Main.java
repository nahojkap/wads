package com.worthlessapps.wads;

import processing.core.PApplet;
import processing.event.KeyEvent;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main extends PApplet
{
    Lock blockManagerLock = new ReentrantLock();

    BlockManager blockManager;

    public void settings(){
        size(800, 800);
    }

    @Override
    public void mouseClicked() {

        blockManagerLock.lock();
        try {
            blockManager = new BlockManager(800, 800, 500);
            blockManager.root = new Block(this, blockManager, 400, 400, 50, 25, BlockDirection.EAST, null);
            blockManager.lastVisibleBlock = blockManager.root;
            blockManager.nextBlockVisibleAt = System.currentTimeMillis() + 100;
            Block block = blockManager.root;
            for (int i = 0; i < 155; i++)
            {
                block.sprout();
                block = block.next();
            }
        }
        finally {
            blockManagerLock.unlock();
        }
    }

    public void draw(){

        background(0);

        blockManagerLock.lock();
        try {
            blockManager.draw();
        }
        finally {
            blockManagerLock.unlock();
        }
    }

    @Override
    public void keyTyped(KeyEvent event) {
        if (event.getKey() == ' ')
        {
            blockManager.active = !blockManager.active;
        }
        super.keyTyped(event);
    }

    public static void main(String... args){

        String[] processingArgs = {"Main"};
        Main mySketch = new Main();
        PApplet.runSketch(processingArgs, mySketch);

    }

}
