package com.worthlessapps.wads;


import processing.core.PApplet;

import java.util.ArrayList;
import java.util.List;

import static com.worthlessapps.wads.BlockDirection.*;

public class Block {

    public static final int BUFFER_SIZE = 1;
    public static final int MINIMUM_HEIGHT = 4;
    public static final int MINIMUM_WIDTH = 4;
    public static final int MAXIMUM_HEIGHT = 50;
    public static final int MAXIMUM_WIDTH = 50;

    boolean visible;

    int x;
    int y;

    int width;
    int height;

    final BlockDirection direction;

    BlockConnection previous;
    BlockConnection next;
    BlockManager blockManager;

    PApplet sketch;

    public Block(PApplet sketch, BlockManager blockManager, int x, int y, int width, int height, BlockDirection direction, BlockConnection previous) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.blockManager = blockManager;
        this.sketch = sketch;
        this.direction = direction;
        this.previous = previous;
    }

    public boolean isOverlapping(Block block) {
        if (block == this) {
            return false;
        }

        if ((block.x + block.height) < x) {
            return false;
        }

        if ((block.y + block.width) < y) {
            return false;
        }

        return true;
    }

    void draw() {

        if (!visible)
        {
            return;
        }

        if (this.previous() == null)
        {
            sketch.fill(255,0,0);
        }
        else
        {
            sketch.fill(102);
        }
        sketch.rect(x, y, width, height);
    }

    // "Sprouts" the next block for this block
    public Block sprout() {
        // Pick a side (not the side which "previous" is connected to)

        final List<BlockDirection> directions = new ArrayList<>(3);
        for (int i = 0; i < BlockDirection.values().length; i++) {
            final BlockDirection blockConnectionDirection = BlockDirection.values()[i];
            if (previous != null && blockConnectionDirection == previous.blockDirection)
                continue;
            directions.add(blockConnectionDirection);
        }

        final BlockDirection chosenBlockConnectionDirection = directions.get((int)sketch.random(3));
        final int nextX;
        final int nextY;
        final int nextWidth;
        final int nextHeight;
        final BlockDirection previousDirection;

        switch (chosenBlockConnectionDirection) {
            case EAST, WEST -> {

                // Previous direction is always opposite of chosen direction to connect at
                previousDirection = chosenBlockConnectionDirection == EAST ? WEST : EAST;

                // Constant will be the X axis
                nextX = chosenBlockConnectionDirection == EAST ? x + width : x;

                // Height is dependent on the height of us (minus our 2 buffers on each side)
                nextHeight = (int)sketch.random(height - 2 * BUFFER_SIZE) + 2*BUFFER_SIZE;

                // Width is random between our minimum and maximum widths
                nextWidth = (int)sketch.random(MINIMUM_WIDTH, MAXIMUM_WIDTH);

                // Y is along the Y axis, at least BUFFER_SIZE from start and BUFFER_SIZE + nextWidth away from end
                nextY = (int)sketch.random(y + BUFFER_SIZE, (y + height) - BUFFER_SIZE - nextHeight);
            }
            case NORTH, SOUTH -> {

                // Previous direction is always opposite of chosen direction to connect at
                previousDirection = chosenBlockConnectionDirection == NORTH ? SOUTH : NORTH;

                // Constant will be the Y axis
                nextY = chosenBlockConnectionDirection == SOUTH ? y + height : y;

                // Width is dependent on the width of us (minus our 2 pixel buffer on each side)
                nextWidth = (int)sketch.random(MINIMUM_WIDTH, width) + 2 * BUFFER_SIZE;

                // Height is random between our minimum and our maximum height
                nextHeight = (int)sketch.random(MINIMUM_HEIGHT, MAXIMUM_HEIGHT);

                // X is along the X axis, at least BUFFER_SIZE from start and BUFFER_SIZE + nextHeight away from end
                nextX = (int)sketch.random(x + BUFFER_SIZE, (x + width) - BUFFER_SIZE - nextWidth);


            }
            default -> {
                throw new RuntimeException("Unhandled direction: " + chosenBlockConnectionDirection);
            }
            // Now what?
        }

        final int finalX = (chosenBlockConnectionDirection == WEST ? nextX - nextWidth : nextX);
        final int finalY = (chosenBlockConnectionDirection == NORTH ? nextY - nextHeight : nextY);

        BlockConnection nextPrevious = new BlockConnection(previousDirection, this);

        Block nextBlock = new Block(sketch, blockManager, finalX, finalY, nextWidth, nextHeight, chosenBlockConnectionDirection, nextPrevious);

        next = new BlockConnection(chosenBlockConnectionDirection, nextBlock);

        return nextBlock;

    }

    public Block next() {
        if (this.next != null) {
            return this.next.connectedBlock;
        }
        return null;
    }

    public Block previous() {
        if (this.previous != null) {
            return this.previous.connectedBlock;
        }
        return null;
    }

    public void setVisible(boolean state)
    {
        this.visible = state;
    }

}
