package com.worthlessapps.wads;

public enum BlockDirection {
    NORTH, EAST, SOUTH, WEST
}
